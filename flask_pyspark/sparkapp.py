from pyspark import SparkContext, SparkConf
from pyspark.sql import SparkSession, SQLContext
from pyspark.sql.functions import *
import json

spark = SparkSession.builder.getOrCreate()
df = spark.read.csv(r"flask_pyspark\3.รายได้เฉลี่ยต่อเดือนต่อครัวเรือน 41-58.csv",header=True,encoding="utf-8")
columnList = df.columns
print(columnList)

def loadDataFromColumn(choosedColumn):
    dfNew = df.select(choosedColumn).collect()
    with open('createjson.json', 'w') as outfile:
        json.dump(choosedColumn,outfile)
        outfile.write("\n")
        json.dump(dfNew, outfile)
    return dfNew
