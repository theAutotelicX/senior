from flask import Flask,request,render_template
import sparkapp 
app = Flask(__name__)


#public var
@app.route('/', methods=['GET','POST'])
def index():  
    global ichooseX
    ichooseX=''
    if request.method == 'POST':
        ichooseX = request.form.getlist('csv')
        #graph here
        ichooseX = sparkapp.loadDataFromColumn(ichooseX)
    htmlForm = Generateform('csv',sparkapp.columnList)
    return render_template('index.html',choice = htmlForm,ichoose=ichooseX)
    
class Generateform:     
    def __init__(self,graphName,columnList):
        self.name = graphName
        self.columnList = columnList 

if __name__ == '__main__':
    app.run()